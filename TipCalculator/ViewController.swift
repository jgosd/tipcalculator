//
//  ViewController.swift
//  TipCalculator
//
//  Created by Gosda Consulting, LLC on 10/30/15.
//  Copyright © 2015 Gosda Consulting, LLC. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var percentage: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        percentage.setTitle("5%", forSegmentAtIndex: 0)
        percentage.setTitle("10%", forSegmentAtIndex: 1)
        percentage.setTitle("15%", forSegmentAtIndex: 2)
        percentage.setTitle("20%", forSegmentAtIndex: 3)
        percentage.setTitle("25%", forSegmentAtIndex: 4)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func checkTotalTextField(sender: UITextField) {
    }

    @IBAction func tipPercentage(sender: UISegmentedControl) {
    }
    
    
    
    
}

